# Template projet

Ce projet vise ... un template pour projet !

En effet, lorsqu'on commence un projet, on se ne sait pas par où commencer, ni qui contacter.

Le but à terme est de fournir un document-notice, qu'on puisse coupler à des ateliers à l'association.

Dossier "images" : les images utilisées pour le document

Dossiers "odt" : le document imprimable, A4, à ouvrir avec Open Office https://www.openoffice.org/fr/

MAJ : le projet est transféré sur cette adresse : https://framagit.org/kelle-fabrik/template-projet
